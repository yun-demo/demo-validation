package com.yun.demo.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Created by qingyun.yu on 2018/8/28.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Component
public @interface ResponseParams {
    String[] params();
}
