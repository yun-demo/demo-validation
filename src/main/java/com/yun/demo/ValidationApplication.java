package com.yun.demo;

/**
 * Created by qingyun.yu on 2018/8/24.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidationApplication {
    public static void main(String... args) {
        SpringApplication.run(ValidationApplication.class, args);
    }
}
