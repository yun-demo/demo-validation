package com.yun.demo.aop;

import com.alibaba.fastjson.JSONObject;
import com.yun.demo.annotation.ResponseParams;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qingyun.yu on 2018/8/28.
 */
@Component
@Aspect
public class ResopnseParamsAop {

    @Pointcut("@annotation(com.yun.demo.annotation.ResponseParams)")
    public void responseParamPointcut() {
    }

    @AfterReturning(value = "responseParamPointcut()&&@annotation(responseParams)", returning = "data")
    public void afterReturning(JSONObject data, ResponseParams responseParams) throws Exception {
        Object obj = data.get("user");
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Map<String, Object> fieldMap = new HashMap<>();
        for (Field field : fields) {
            field.setAccessible(true);
            String key = field.getName();
            Object value = field.get(obj);
            fieldMap.put(key, value);
        }
        Map<String, Object> resultMap = new HashMap<>();
        String[] returnParams = responseParams.params();
        for (String returnParam : returnParams) {
            resultMap.put(returnParam, fieldMap.get(returnParam));
        }
        data.put("user", resultMap);
    }
}
