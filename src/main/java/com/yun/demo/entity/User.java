package com.yun.demo.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * Created by qingyun.yu on 2018/8/24.
 */
public class User implements Serializable {
    private static final long serialVersionUID = -8591036914874515958L;
    @NotBlank(message = "{message.user.name}")
    private String name;

    @NotBlank(message = "{message.user.phone.null}")
    @Length(min = 11, max = 11, message = "{message.user.phone.length}")
    @Digits(integer = 11, fraction = 0, message = "{message.user.phone.digit}")
    private String phone;

    @NotNull(message = "{message.user.email.null}")
    @Email(regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", message = "{message.user.email.format}")
    //@Pattern(regexp = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$", message = "{message.user.email.format}")
    private String email;

    @Min(value = 0, message = "{message.user.age.min}")
    @Max(value = 100, message = "{message.user.age.max}")
    private int age;

    @NotNull(message = "{message.user.url.null}")
    @URL(regexp = "^([hH][tT]{2}[pP]:/*|[hH][tT]{2}[pP][sS]:/*|[fF][tT][pP]:/*)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\\\/])+(\\\\?{0,1}(([A-Za-z0-9-~]+\\\\={0,1})([A-Za-z0-9-~]*)\\\\&{0,1})*)$", message = "{message.user.url.format}")
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
