package com.yun.demo.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolationException;

/**
 * Created by qingyun.yu on 2018/8/24.
 */
@ControllerAdvice
public class ExceptionController {
    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    public JSONObject constraintViolationExceptionHandler(ConstraintViolationException exception) {
        JSONObject data = new JSONObject();
        data.put("exception", exception.getMessage());
        return data;
    }
    @ResponseBody
    @ExceptionHandler(BindException.class)
    public JSONObject bindExceptionHandler(BindException bindException) {
        JSONObject data = new JSONObject();
        data.put("message", bindException.getMessage());
        return data;
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public JSONObject exceptionHandler(Exception exception) {
        JSONObject data = new JSONObject();
        data.put("exception", exception.getMessage());
        return data;
    }
}
