package com.yun.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.yun.demo.annotation.ResponseParams;
import com.yun.demo.entity.User;
import com.yun.demo.validated.annotation.TokaValidated;
import com.yun.demo.validated.annotation.ValiForm;
import com.yun.demo.validated.enums.ValidatedType;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * Created by qingyun.yu on 2018/8/24.
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseParams(params = {"name", "phone"})
    @TokaValidated({@ValiForm(param = "name", message = "${message.user.name}")
            , @ValiForm(param = "name", message = "email format", type = ValidatedType.EMAIL)
            , @ValiForm(param = "pageSize", value = "10", type = ValidatedType.MAX, message = "pageSize < 10")})
    public JSONObject create(Integer pageSize, @RequestBody User user) {
        JSONObject data = new JSONObject();
        data.put("user", user);
        data.put("pageSize", pageSize);
        return data;
    }

    @RequestMapping(value = "/list", method = RequestMethod.PATCH)
    @ResponseBody
    public JSONObject list(String name) {
        JSONObject data = new JSONObject();
        data.put("status", "ok");
        return data;
    }
}
