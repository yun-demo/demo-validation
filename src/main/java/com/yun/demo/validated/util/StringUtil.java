package com.yun.demo.validated.util;

import com.yun.demo.validated.exception.TokaParamException;

import java.util.regex.Pattern;


/**
 * Created by Administrator on 2018/9/17.
 */
public class StringUtil {

    public static boolean isDigit(String arg) {
        if(arg.length() == 0 || arg.length() == 1 && "-".equals(arg.substring(0, 1))) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*.?[\\d]*$");
        return pattern.matcher(arg).matches();
    }

}
