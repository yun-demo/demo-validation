package com.yun.demo.validated.util;

import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.exception.TokaParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Qingyun Yu
 * @Description
 * @create 2018-04-25 13:54
 **/
public class ReflectionUtil {
    private static Logger logger = LoggerFactory.getLogger(ReflectionUtil.class);

    public static Object getFieldValue(Object obj, String fieldName) {
        Object value = null;
        try {
            Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            value = field.get(obj);
        } catch (IllegalAccessException e) {
            logger.error("get private field failed,Exception:{}", e.toString());
        } catch (NoSuchFieldException e) {
            logger.error("get private field failed,Exception:{}", e.toString());
        }
        return value;
    }

    public static String[] getParameterNames(Class clazz, String methodName, Class... paramTypes) {
        String[] parameterNames = null;
        ParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();
        try {
            parameterNames = parameterNameDiscoverer.getParameterNames(clazz.getDeclaredMethod(methodName, paramTypes));
        } catch (NoSuchMethodException e) {
            logger.error("get parameter name failed,Exception:{}", e.toString());
        }
        return parameterNames;
    }

    public static Map<String,Object> getBeanValue(Object obj) {
        if(Objects.isNull(obj)) {
            throw new TokaParamException("obj is null");
        }
        Class clazz = obj.getClass();
        Field[] fields = clazz.getFields();
        Map<String,Object> resultMap = new HashMap<String, Object>();
        for(Field field: fields) {
            resultMap.put(field.getName(), getFieldValue(obj, field.getName()));
        }
        return resultMap;
    }

    public static void getObjectFields(Class clazz, Object obj, Map<String, ParamInfo> paramInfoMap) {
        Class superClass = clazz.getSuperclass();
        if (!"Object".equals(superClass.getSimpleName())) {
            getObjectFields(superClass, obj, paramInfoMap);
        }
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object value = field.get(obj);
                if (value == null) {
                    ParamInfo paramInfo = new ParamInfo(field.getType(), null);
                    paramInfoMap.put(field.getName(), paramInfo);
                } else {
                    ParamInfo paramInfo = new ParamInfo(field.getType(), value);
                    paramInfoMap.put(field.getName(), paramInfo);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
