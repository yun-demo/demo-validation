package com.yun.demo.validated.util;

import com.yun.demo.validated.exception.TokaParamException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Created by Administrator on 2018/9/17.
 */
public class VerifyUtil {
    public static void isDigit(String value, String message) {
        if(!StringUtil.isDigit(value)) {
            throw new TokaParamException(message);
        }
    }

    public static void verifyObjectNotNull(Object obj, String message) {
        if(Objects.isNull(obj)) {
            throw new TokaParamException(message);
        }
    }

    public static void verifyIsString(Class clazz) {
        if(!"String".equals(clazz.getSimpleName())) {
            throw new TokaParamException("type is not String");
        }
    }

    public static void verifySize(Object objA, Object objB, Class clazz, String message) {
        switch (clazz.getSimpleName()) {
            case "Integer": {
                if(Integer.valueOf(objA.toString()) < Integer.valueOf(objB.toString())) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            case "Long": {
                if(Long.valueOf(objA.toString()) < Long.valueOf(objB.toString())) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            case "Short": {
                if(Short.valueOf(objA.toString()) < Short.valueOf(objB.toString())) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            case "Float": {
                if(Float.valueOf(objA.toString()) < Float.valueOf(objB.toString())) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            case "Double": {
                if(Double.valueOf(objA.toString()) < Double.valueOf(objB.toString())) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            case "BigInteger": {
                if(new BigInteger(objA.toString()).compareTo(new BigInteger(objB.toString())) < 0) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            case "BigDecimal": {
                if(new BigDecimal(objA.toString()).compareTo(new BigDecimal(objB.toString())) < 0) {
                    throw new TokaParamException(message);
                } else {
                    return;
                }
            }
            default: throw new TokaParamException("Illegal type");
        }
    }
}
