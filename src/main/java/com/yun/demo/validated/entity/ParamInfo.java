package com.yun.demo.validated.entity;

/**
 * Created by Administrator on 2018/9/17.
 */
public class ParamInfo {
    private Class paramType;
    private Object paramValue;

    public ParamInfo(Class paramType, Object paramValue) {
        this.paramType = paramType;
        this.paramValue = paramValue;
    }

    public Class getParamType() {
        return paramType;
    }

    public Object getParamValue() {
        return paramValue;
    }
}
