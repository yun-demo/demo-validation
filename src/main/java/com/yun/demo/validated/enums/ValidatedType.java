package com.yun.demo.validated.enums;

import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.handler.*;

/**
 * Created by Administrator on 2018/9/16.
 */
public enum ValidatedType {
    STR_NON_BLANK(new StrNonBlankHandler()),
    OBJ_NON_NULL(new ObjNonNull()),
    STR_IS_DIGIT(new StrIsDigit()),
    STR_MAX_LENGTH(new StrMaxLength()),
    STR_MIN_LENGTH(new StrMinLength()),
    EMAIL(new Email()),
    PATTERN(new Pattern()),
    URL(new URL()),
    MIN(new Min()),
    MAX(new Max()),
    LENGTH(new Length());

    ValidatedType(BaseHandler baseHandler) {
        this.baseHandler = baseHandler;
    }
    private BaseHandler baseHandler;

    public void handler(ParamInfo paramInfo, String value, String message) {
        this.baseHandler.handler(paramInfo, value, message);
    }
}
