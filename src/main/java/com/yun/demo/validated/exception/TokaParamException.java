package com.yun.demo.validated.exception;

/**
 * Created by Administrator on 2018/9/16.
 */
public class TokaParamException extends RuntimeException {
    public TokaParamException(String message) {
        super(message);
    }
}
