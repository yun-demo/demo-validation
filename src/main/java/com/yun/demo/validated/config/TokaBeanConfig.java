package com.yun.demo.validated.config;

import com.yun.demo.validated.util.ResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by Administrator on 2018/9/17.
 */
@Configuration
public class TokaBeanConfig {
    @Bean("validationMessages")
    public static Properties properties() throws IOException {
        Properties properties = new Properties();
        ResourceLoader resourceLoader = new ResourceLoader("ValidationMessages.properties");
        properties.load(resourceLoader.getInputStream());
        return properties;
    }
}
