package com.yun.demo.validated.aop;

import com.yun.demo.validated.annotation.TokaValidated;
import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.exception.TokaParamException;
import com.yun.demo.validated.handler.BaseHandler;
import com.yun.demo.validated.util.ObjectUtil;
import com.yun.demo.validated.util.ReflectionUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.ProxyMethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;


/**
 * Created by Administrator on 2018/9/16.
 */
@Component
@Aspect
public class TokaValidatedAop {
    @Resource(name = "validationMessages")
    private Properties properties;

    @Pointcut("@annotation(com.yun.demo.validated.annotation.TokaValidated)")
    public void tokaValidatedPointcut() {}

    @Before("tokaValidatedPointcut()&&@annotation(tokaValidated)")
    public void before(JoinPoint joinPoint, TokaValidated tokaValidated) {
        ProxyMethodInvocation methodInvocation = (ProxyMethodInvocation) ReflectionUtil.getFieldValue(joinPoint, "methodInvocation");
        Class clazz = joinPoint.getTarget().getClass();
        Method method = methodInvocation.getMethod();
        Class[] paramTypes = method.getParameterTypes();
        String[] paramNames = ReflectionUtil.getParameterNames(clazz, method.getName(), paramTypes);
        Object[] args = joinPoint.getArgs();
        Map<String, ParamInfo> paramInfoMap = new HashMap<>();
        for(int i = 0; i < paramNames.length; i++) {
            ParamInfo paramInfo = new ParamInfo(paramTypes[i], args[i]);
            paramInfoMap.put(paramNames[i], paramInfo);
            if(ObjectUtil.isObject(paramTypes[i])) {
                ReflectionUtil.getObjectFields(paramTypes[i], args[i], paramInfoMap);
            }
        }
        Stream.of(tokaValidated.value()).forEach(valiForm -> {
            valiForm.type().handler(paramInfoMap.get(valiForm.param()), valiForm.value(), this.getMessage(valiForm.message()));
        });
        Stream.of(tokaValidated.divForms()).forEach(divForm -> {
            Class handlerClass = divForm.handler();
            try {
                BaseHandler baseHandler = (BaseHandler)handlerClass.newInstance();
                baseHandler.handler(paramInfoMap.get(divForm.param()), divForm.value(), this.getMessage(divForm.message()));
            } catch (InstantiationException e) {
                throw new TokaParamException(e.getMessage());
            } catch (IllegalAccessException e) {
                throw new TokaParamException(e.getMessage());
            }
        });
    }

    private String getMessage(String message) {
        return message.contains("$")? properties.getProperty(message.substring(2, message.length() - 1)): message;
    }
}
