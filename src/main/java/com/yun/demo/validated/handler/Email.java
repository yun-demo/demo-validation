package com.yun.demo.validated.handler;

import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.exception.TokaParamException;
import com.yun.demo.validated.util.ObjectUtil;
import com.yun.demo.validated.util.VerifyUtil;

import java.util.Objects;

/**
 * Created by Administrator on 2018/9/17.
 */
public class Email implements BaseHandler {
    private String emailPattern = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$";
    @Override
    public void handler(ParamInfo paramInfo, String value, String message) {
        if(Objects.isNull(paramInfo.getParamValue())) {
            return;
        }
        VerifyUtil.verifyIsString(paramInfo.getParamType());
        if("".equals(value)) {
            value = emailPattern;
        }
        if(!((String)paramInfo.getParamValue()).matches(value)) {
            throw new TokaParamException(message);
        }
    }
}
