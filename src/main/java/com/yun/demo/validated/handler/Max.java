package com.yun.demo.validated.handler;

import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.util.VerifyUtil;

import java.util.Objects;

/**
 * Created by Administrator on 2018/9/18.
 */
public class Max implements BaseHandler {
    @Override
    public void handler(ParamInfo paramInfo, String value, String message) {
        if (Objects.isNull(paramInfo.getParamValue())) {
            return;
        }
        VerifyUtil.isDigit(value, "value is non digit");
        VerifyUtil.verifySize(value, paramInfo.getParamValue(), paramInfo.getParamType(), message);
    }
}
