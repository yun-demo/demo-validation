package com.yun.demo.validated.handler;

import com.google.common.base.Strings;
import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.exception.TokaParamException;
import com.yun.demo.validated.util.ObjectUtil;
import com.yun.demo.validated.util.VerifyUtil;

/**
 * Created by Administrator on 2018/9/16.
 */
public class StrNonBlankHandler implements BaseHandler {
    @Override
    public void handler(ParamInfo paramInfo, String value, String message) {
        VerifyUtil.verifyIsString(paramInfo.getParamType());
        if(Strings.isNullOrEmpty((String)paramInfo.getParamValue())) {
            throw new TokaParamException(message);
        }
    }
}
