package com.yun.demo.validated.handler;

import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.exception.TokaParamException;
import com.yun.demo.validated.util.VerifyUtil;

import java.util.Objects;

/**
 * Created by Administrator on 2018/9/18.
 */
public class URL implements BaseHandler {
    private String urlPattern = "^([hH][tT]{2}[pP]:/*|[hH][tT]{2}[pP][sS]:/*|[fF][tT][pP]:/*)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+(\\?{0,1}(([A-Za-z0-9-~]+\\={0,1})([A-Za-z0-9-~]*)\\&{0,1})*)$";
    @Override
    public void handler(ParamInfo paramInfo, String value, String message) {
        if (Objects.isNull(paramInfo.getParamValue())) {
            return;
        }
        VerifyUtil.verifyIsString(paramInfo.getParamType());
        if("".equals(value)) {
            value = urlPattern;
        }
        if(!((String)paramInfo.getParamValue()).matches(value)) {
            throw new TokaParamException(message);
        }
    }
}
