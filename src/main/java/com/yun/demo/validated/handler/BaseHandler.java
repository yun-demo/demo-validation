package com.yun.demo.validated.handler;

import com.yun.demo.validated.entity.ParamInfo;

/**
 * Created by Administrator on 2018/9/16.
 */
public interface BaseHandler {
    void handler(ParamInfo paramInfo, String value, String message);
}
