package com.yun.demo.validated.handler;

import com.yun.demo.validated.entity.ParamInfo;
import com.yun.demo.validated.exception.TokaParamException;
import com.yun.demo.validated.util.VerifyUtil;

import java.util.Objects;

/**
 * Created by Administrator on 2018/9/17.
 */
public class ObjNonNull implements BaseHandler {
    @Override
    public void handler(ParamInfo paramInfo, String value, String message) {
        VerifyUtil.verifyObjectNotNull(paramInfo.getParamValue(), message);
    }
}
