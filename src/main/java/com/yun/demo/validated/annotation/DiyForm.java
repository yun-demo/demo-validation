package com.yun.demo.validated.annotation;



import com.yun.demo.validated.handler.StrNonBlankHandler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Administrator on 2018/9/17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface DiyForm {
    String param();
    String value() default "";
    String message() default "";
    Class handler() default StrNonBlankHandler.class;
}
