package com.yun.demo.validated.annotation;

import com.yun.demo.validated.enums.ValidatedType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Administrator on 2018/9/16.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface ValiForm {
    String param();
    String value() default "";
    String message() default "";
    ValidatedType type() default ValidatedType.STR_NON_BLANK;
}
